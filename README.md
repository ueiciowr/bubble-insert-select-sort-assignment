### Documentação

__Para executar o projeto, faça um clone do repositório com:__

```shell
git clone https://gitlab.com/ueiciowr/bubble-insert-select-sort-assignment.git
```

__Ou, baixe o projeto compactado, a partir do botão Download do GitLab.__

---

__O projeto foi criado com o Maven, a partir do Visual Studio Code, para executar baixe as seguintes dependências:__

- [Visual Studio Code](https://code.visualstudio.com)

- [Java Extension Pack](https://marketplace.visualstudio.com/items?itemName=vscjava.vscode-java-pack)

- [Language Support for Java(TM) by Red Hat](https://marketplace.visualstudio.com/items?itemName=redhat.java)

- [Java Language Support](https://marketplace.visualstudio.com/items?itemName=georgewfraser.vscode-javac)

- [Java Run](https://marketplace.visualstudio.com/items?itemName=caolin.java-run)

---

#### Configure o path do java no Visual Studio Code, nas Settings, procure por __`Java Home`__, ou adicione nas settings.json o seguinte código, com o referente path para o Java Development Kit:

```json
{
  "java.home": "C:\\Program Files\\Java\\jdk1.8.0_231",
}
```

---

__Feito isso, o projeto estará no caminho:__

```text
2/src/main/java/com/person
```

__Para executá-lo, procure por `FileWriter archive` em `Contatos.java` e altere o path que salva a ordenação em algum arquivo existente no seu computador__

__Note que, a barra de diretórios no Windows é `\\`, no Linux é `/`__

```java
public void imprimeContatos(double time) throws Exception {
  FileWriter archive = new FileWriter("C:\\Users\\Seu_Usuario\\Desktop\\orderListAlgorithm.txt");
}
```

---

__Deverá aparecer, na classe principal `OrdenaObjetoApp`, logo abaixo do nome da classe, `run` e `debug`. Execute, run!__
